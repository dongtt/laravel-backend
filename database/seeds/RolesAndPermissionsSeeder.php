<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $defaultRoleNames = config('permission.default_role_names');

        // create permissions
        Permission::create(['name' => 'user.*', 'module' => 'user']);
        Permission::create(['name' => 'user.edit', 'module' => 'user']);
        Permission::create(['name' => 'user.delete', 'module' => 'user']);
        Permission::create(['name' => 'setting.permission', 'module' => 'setting']);

        foreach($defaultRoleNames as $role) {
            Role::create(['name' => $role]);
        }

        // $superAdminRole->syncPermissions([Permission::all()->pluck('name')->toArray()]);

        $superUser = App\Models\User::whereEmail('super@dongtt.com')->first();
        $superUser->assignRole($defaultRoleNames['super_admin']);

        $poster = App\Models\User::whereEmail('admin@dongtt.com')->first();
        $poster->assignRole($defaultRoleNames['admin']);
        sleep(2);
        // // create roles and assign created permissions

        // // this can be done as separate statements
        // $role = Role::create(['name' => 'writer']);
        // $role->givePermissionTo('edit articles');

        // // or may be done by chaining
        // $role = Role::create(['name' => 'moderator'])
        //     ->givePermissionTo(['publish articles', 'unpublish articles']);

        // $role = Role::create(['name' => 'superadmin']);

        factory(App\Models\Role::class, 100)->create();
    }
}
