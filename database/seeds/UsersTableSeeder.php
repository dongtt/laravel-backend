<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Adminstrator',
            'email' => 'super@dongtt.com',
            'password' => Hash::make('123456'),
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@dongtt.com',
            'password' => Hash::make('123456'),
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);

        User::create([
            'name' => 'Member',
            'email' => 'member@dongtt.com',
            'password' => Hash::make('123456'),
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);
    }
}
