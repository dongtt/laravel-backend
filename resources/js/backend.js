/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

import Vue from 'vue';
import VueProgressBar from "vue-progressbar";
import VueAxios from 'vue-axios';
import axios from 'axios';

import vuetify from './vuetify';
import router from './router';
import axiosInterceptors from './core/axios-interceptors';
import BackendApplicationView from './layouts/BackendApplicationView';

Vue.use(VueAxios, axios)
Vue.use(VueProgressBar, {
    color: "var(--md-theme-default-accent, #ff5722)",
    failedColor: "red",
    height: "2px"
});

Vue.component('BackendApplication', BackendApplicationView);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const backendApp = new Vue({
    el: '#backend-page',
    vuetify,
    router,
});

axiosInterceptors(backendApp);
