import CODE from './code';
import Vue from 'vue';

export default (app) => {
    Vue.axios.interceptors.request.use(config => {
        // for every request start the progress
        app.$Progress.start();

        return config;
    });

    Vue.axios.interceptors.response.use(response => {
        // finish when a response is received
        app.$Progress.finish();

        return response;
    }, (error) => {
        app.$Progress.finish();

        if (error.response && error.response.status === CODE.ERROR_UNAUTHORIZED) {
            return window.location.href = '/admin';
        }

        return Promise.reject(error)
    });
};
