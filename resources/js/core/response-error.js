class ResponseError {
    constructor(message, fields = []) {
        this.message = message;
        this.fields = {};
        fields.forEach(f => {
            this.fields[f] = []
        })
    }

    static create(message, fields = []) {
        return new ResponseError(message, fields);
    }

    /**
     * Convert response error to error map
     *
     * @param HttpResponse response
     *
     * @return ResponseError
     *
     */
    parse(errors) {
        Object.keys(errors).forEach(k => {
            this.fields[k] = errors[k]
        });

        return this;
    }

    map(response) {
        this.message = response?.data?.message || response?.statusText || 'Unknow Error';
        this.parse(response?.data?.errors || {});
    }

    mapToVuetifyRules() {
        const rules = {};
        Object.keys(this.fields).forEach(field => {
            rules[field] = this.fields[field].map(msg => (() => msg || true));
        });

        return rules;
    }
}

export default ResponseError;
