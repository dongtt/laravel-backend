/**
 * Http Response status
 *
 * @const Object
 */
export default {
    // OK
    SUCCESS_OK: 200,
    // Object created
    SUCCESS_CREATED: 201,
    // No content
    SUCCESS_NO_CONTENT: 204,
    //Partial content: usually use when pagination resouce
    SUCCESS_PARTIAL_CONTENT: 206,

    //Bad request:  fail to pass validation.
    ERROR_BAD_REQUEST: 400,
    // Unauthorized
    ERROR_UNAUTHORIZED: 401,
    // Forbidden: does not have the permissions
    ERROR_FORBIDDEN: 403,
    // Not found
    ERROR_NOT_FOUND: 404,
    // Unprocessable Entity (validation failed)
    ERROR_UNPROCESSABLE_ENTITY: 422,
    // Internal Server error
    ERROR_INTERNAL_SERVER: 500,
    // Service unavailable
    ERROR_SERVICE_UNAVAILABLE: 503
}
