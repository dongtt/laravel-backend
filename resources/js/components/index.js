import AlertDialog from './AlertDialog';
import ConfirmDialog from './ConfirmDialog';

export { AlertDialog, ConfirmDialog };
