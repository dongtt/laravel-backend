
import {
    DashboardComponent,
    UserManagementComponent,
    NotFoundComponent,
} from '@/pages';

const routes = [
    {
        path: '/admin',
        redirect: 'admin/dashboard'
    },
    {
        path: '/admin/dashboard',
        name: 'Dashboard',
        component: DashboardComponent
    },
    {
        path: '/admin/users',
        name: 'User Management',
        component: UserManagementComponent
    },
    {
        path: '/admin/roles',
        name: 'Role Management',
        component: () => import('@/pages/role/RoleManagementComponent.vue')
    },
    {
        path: '*',
        name: 'Not Found',
        component: NotFoundComponent,
    }
];

export default routes;
