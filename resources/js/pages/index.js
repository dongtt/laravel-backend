import DashboardComponent from './DashboardComponent';
import UserManagementComponent from './UserManagementComponent';
import RoleManagementComponent from './role/RoleManagementComponent';
import NotFoundComponent from './NotFoundComponent';

export {
    DashboardComponent,
    UserManagementComponent,
    NotFoundComponent,
    RoleManagementComponent
};
