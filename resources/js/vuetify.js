import Vue from 'vue';
import Vuetify from 'vuetify';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify)

const opts = {
    theme: {
        themes: {
            light: {
                ...colors,
                primary: colors.teal,
                secondary: colors.teal.lighten3,
                accent: colors.orange,
                success: colors.green,
                warning: colors.yellow.accent2,
                error: colors.red,
            }
        },
    }
}

export default new Vuetify(opts)
