<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel Adminstrator') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('assets/css/backend.css') }}" rel="stylesheet">
    <style>
        /* #backend-page {
            height: 100vh;
            width: 100vw;
        } */
    </style>
    @yield('styles')
</head>

<body>
    <div id="backend-page">
        @yield('content')
    </div>

    <!-- Scripts -->
    @yield('script')
    <script src="{{ asset('assets/js/backend.js') }}" defer></script>
</body>

</html>
