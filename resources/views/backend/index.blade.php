@extends('layouts.backend')

@section('content')
<router-view></router-view>
@endsection
@section('script')
<script>
    window.authUser = {!! auth()->user()->toJson()!!};
</script>
@endsection
