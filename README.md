<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Laravel Engine
- Eloquent Model
- Laravel Resource
- Laravel UI (vue)

## Laravel and PHP packages

- **[Roles: spatie/laravel-permission](https://github.com/spatie/laravel-permission/)**

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## Environment

- PHP: version >= 7.2.5
- SQL: support JSON

## Configuration

# Roles:

- Config in ``` config/permission ```

## License

[MIT license](https://opensource.org/licenses/MIT).
