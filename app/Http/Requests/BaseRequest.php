<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseRequest extends FormRequest
{
    /**
     * Singature of model
     *
     * @var String
     */
    protected $signature;

    public function __construct()
    {
        $this->signature = $this->loadSignature();
    }

    /**
     * Singature of model defination
     *
     * @var String
     */
    abstract protected function loadSignature();

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    // /**
    //  * Get data to be validated from the request.
    //  *
    //  * @return array
    //  */
    // public function validationData()
    // {
    //     return array_merge($this->all(), [$this->signature => $this->route($this->signature)]);
    // }
}
