<?php

namespace App\Http\Requests;

// use Illuminate\Validation\Rule;
use App\Models\Role;

class RoleRequest extends BaseRequest
{
    /**
     * Singature of model defination
     *
     * @var String
     */
    protected function loadSignature()
    {
        return 'role';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() === 'PATCH') {
            return [
                'ids' => 'required|array',
                'ids.*' => 'exists:App\Models\Role,id'
            ];
        }

        $rule = $this->route($this->signature);

        return [
            'name' => [
                'required',
                'string',
                'max:30',
                'unique:App\Models\Role,name' . ($rule ? ",$rule->id" : ''),
                // Rule::notIn(config('permission.default_role_names')),
            ]
        ];
    }
}
