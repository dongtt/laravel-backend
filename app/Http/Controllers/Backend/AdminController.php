<?php

namespace App\Http\Controllers\Backend;

class AdminController extends BackendController
{
    public function index()
    {
        return view('backend.index');
    }
}
