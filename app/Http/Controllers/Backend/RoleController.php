<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\RoleResource;
use App\Http\Resources\RolesCollection;
use App\Models\Role;

class RoleController extends BackendController
{
    public function __construct()
    {
        $this->authorizeResource(Role::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return new RolesCollection(Role::ordered()->paginate((int)$request->take ?: 12));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\RoleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $role = new RoleResource(
            Role::create($request->only(['name']) + ['guard_name' => 'web'])
        );

        return response()->json(compact('role'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return response()->json(['role' => new RoleResource($role)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\RoleRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->only(['name']));

        return response()->json(['role' => new RoleResource($role)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return response()->json(null, self::SUCCESS_NO_CONTENT);
    }

    public function destroyMany(RoleRequest $request)
    {
        Role::whereIn('id', $request->input('ids'))->delete();

        return response()->json(null, self::SUCCESS_NO_CONTENT);
    }
}
