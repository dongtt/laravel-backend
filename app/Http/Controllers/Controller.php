<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * =========================================
     * Http Response status
     *
     * @const Integer
     */

    // OK
    const SUCCESS_OK = 200;
    // Object created
    const SUCCESS_CREATED = 201;
    // No content
    const SUCCESS_NO_CONTENT = 204;
    //Partial content: usually use when pagination resouce
    const SUCCESS_PARTIAL_CONTENT = 206;

    //Bad request: fail to pass validation.
    const ERROR_BAD_REQUEST = 400;
    // Unauthorized
    const ERROR_UNAUTHORIZED = 401;
    // Forbidden:does not have the permissions
    const ERROR_FORBIDDEN = 403;
    // Not found
    const ERROR_NOT_FOUND = 404;
    // Unprocessable Entity (validation failed)
    const ERROR_UNPROCESSABLE_ENTITY = 422;
    // Internal Server error
    const ERROR_INTERNAL_SERVER = 500;
    // Service unavailable
    const ERROR_SERVICE_UNAVAILABLE = 503;
}
