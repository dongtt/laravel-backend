<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;

class AdminAuthenticated
{
    public function handle($request, \Closure $next)
    {
        if (!\Auth::user() || !\Auth::user()->isAdmin()) {
            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
