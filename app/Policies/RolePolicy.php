<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any roles.
     *
     * @param  \App\Models\User  $user
     * @return Boolean
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $role
     * @return Boolean
     */
    public function view(User $user, Role $role)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\Models\User  $user
     * @return Boolean
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $role
     * @return Boolean
     */
    public function update(User $user, Role $role)
    {
        return $user->isAdmin() && !$role->isDefaultRole();
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $role
     * @return Boolean
     */
    public function delete(User $user, Role $role)
    {
        return $user->isAdmin() && !$role->isDefaultRole();
    }

    /**
     * Determine whether the user can permanently delete the role.
     *
     * @param  \App\Models\User  $user
     * @return Boolean
     */
    public function destroyMany(User $user)
    {
        $roles = Role::whereIn('id', request()->only('ids'))->get()->pluck('name')->toArray();

        $intersect = array_intersect($roles, config('permission.default_role_names'));

        return $user->isAdmin() &&  count($intersect) === 0;
    }
}
