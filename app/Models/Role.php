<?php

namespace App\Models;

use Spatie\Permission\Models\Role as SpatieRole;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\UsesUuid;
use App\Models\Traits\QueryScope;

class Role extends SpatieRole
{
    use SoftDeletes, UsesUuid, QueryScope;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'pivot'];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Check role is default roles
     *
     * @param \App\Models\Role  $role
     * @return boolean
     */
    public function isDefaultRole()
    {
        return in_array($this->name, config('permission.default_role_names'));
    }
}
