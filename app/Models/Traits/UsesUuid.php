<?php

namespace App\Models\Traits;

trait UsesUuid
{
    /**
     * Overide boot model to create Uuid for primary key
     *
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) \Str::uuid();
        });
    }
}
