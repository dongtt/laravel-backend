<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Backend Ajax Request Routes
|--------------------------------------------------------------------------
|
| Here is where you can register backend routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::get('my/profile', 'UserController@getMyProfile');

Route::resource('users', 'UserController')->only(['index', 'store', 'show', 'update', 'destroy']);

// Route::patch('roles', 'RoleController@destroyMany');
Route::resource('roles', 'RoleController')->only(['index', 'store', 'show', 'update', 'destroy', 'destroyMany']);
